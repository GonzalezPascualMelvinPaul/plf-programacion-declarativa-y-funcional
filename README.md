## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001).


```plantuml
@startmindmap
*[#EE8572] Programación.
	*_ De tipo

		*[#63B7AF] Declarativa.
			*_ Trata
				*[#ABF0E9] De un estilo de programación.
				*[#ABF0E9] De dar enfasis en los\naspectos creativos de la programacion.
				*[#ABF0E9] En abandonar las secuencias de órdenes.
					*_ Gestionan
						*[#D5DBB3] La misma memoria del ordenador.
						*[#D5DBB3] La perspectiva para dar mas explicaciones.
			*[#ABF0E9] Propósitos
				*_ Son
					*[#D5DBB3] Liberarse de las asignaciones, de detallar el\ncontrol de la gestión de memoria en el ordenador.
					*[#D5DBB3] Reducir la complejidad de los programas.
					*[#D5DBB3] Evitar el riesgo de cometer\nerrores haciendo programas.

			*[#ABF0E9] Consigue
				*_ Programas más 
					*[#D5DBB3] Cortos.
					*[#D5DBB3] Fácil de realizar.
					*[#D5DBB3] Fácil de depurar.

			*[#ABF0E9] Ventajas.
				*[#D5DBB3] Tiempo de desarrollo de\nuna aplicacion es menor.
				*[#D5DBB3] Tiempo de modificacion\ny depuracion es menor.
			*[#ABF0E9] Compilador.
				*_ Es el
					*[#F7F3E9] Encargado de la gestion de memoria.
				*[#D5DBB3] Ventajas.
					*[#F7F3E9] Mejor perspectiva.
					*[#F7F3E9] Lenguajes declarativos.
					*[#F7F3E9] Tiempo de modificación.
					*[#F7F3E9] Tiempo de verificación.			
			*[#ABF0E9] Variantes
				*_ De tipo
					*[#D5DBB3] Funcional.
						*_ Características.
							*[#F7F3E9] Funciones de ordenes superior.
							*[#F7F3E9] No hay distincion entre datos y programas.
							*[#F7F3E9] Evaluación perezosa.
							*[#F7F3E9] Modelo de la demostracion de la logica\ny de la demostracion automatica.
							
					*[#D5DBB3] Lógica.
						*_ Características
							*[#F7F3E9] Son predicados de primer orden.
							*[#F7F3E9] No establece un orden entre\nargumentos de entrada y datos de salida.
					
						*_ Ventajas.
							*[#F7F3E9] Permite ser mas declarativo.
							*[#F7F3E9] Establece la direccion\nde procesamiento mediante\nargumentos y resultados.
			*[#ABF0E9] Antecedentes
				*_ Surgue
					*[#D5DBB3] Problemas de la programacion clasica.
			*_ Tiene 
				*[#ABF0E9] Dos partes
					*_ Que son
						*[#D5DBB3] Creativa.
							*[#F7F3E9] Algoritmo.
							*[#F7F3E9] Forma de resolver un problema.
							*[#F7F3E9] Ahorro de tiempo.
							*[#F7F3E9] Forma de ahorrar\nmemoria al resolver el problema.
						*[#D5DBB3] Burocrática.
							*[#F7F3E9] Parte en que se realiza\nla gestion detallada de la memoria.
							*[#F7F3E9] Secuencia esctricta de\nordenes que llevan a una solucion deseada.
					*_ Existe
						*[#D5DBB3] Un equilibiro entre la creativa y burocratica.
							*_ Depende
								*[#F7F3E9] Del tipo y lenguaje de programacion que se utilize.
									*_ Ejemplo
										* Código máquina.
		*[#63B7AF] Imperativa.
			*_ Es
				*[#ABF0E9] Obligado a dar demasidos detalles\nsobre los calculos que se desean realizar. 
			*_ Tipos
				*[#ABF0E9] Estructurada.
				*[#ABF0E9] Procedimental.
				*[#ABF0E9] Modular.
			*_ Lenguajes.
				*[#ABF0E9] Fortran.
				*[#ABF0E9] C.
				*[#ABF0E9] Pascal.
			*_ Características.
				*[#ABF0E9] Cuellos de botellas.
				*[#ABF0E9] Ideas abstractas.
				*[#ABF0E9] Dependen de tipo programa.
		*[#63B7AF] Operativa.
			*_ Es
				*[#ABF0E9] Basado en el modelo Von Neumann.
		*[#63B7AF] Orientada a la inteligencia artificial.
			*_ Se
				*[#ABF0E9] Estudia como un modelo de programacion imperativa.
			*_ Utiliza
				*[#ABF0E9] LISP.
					*_ Es
						*[#D5DBB3] Un lenguaje hibrido, permite un tipo de programacion imperativa.
				*[#ABF0E9] HASKEL.
					*_ Es
						*[#D5DBB3] Un lenguaje estandar de lenguaje de programacion.
				*[#ABF0E9] PROLOG.
					*_ Características
						*[#D5DBB3] Evita el solapamiento excesivo.
						*[#D5DBB3] Representacion del paradigma declarativo.
	*[#63B7AF] Evolución.
		*_ Se
			*[#ABF0E9] Tenia que hablar en código máquina.
		*_ Secuencias de ordenes
			*[#ABF0E9] Directas.
			*[#ABF0E9] Concretas.
			*[#ABF0E9] Sencillas.
		*_ Desarrollaron
			*[#ABF0E9] Lenguajes de alto nivel.
				*_ Por ejemplo
					*[#D5DBB3] FORTRAN.

	*[#63B7AF] Instrumento esencial de
		*_ La programación
			*[#ABF0E9] Es la asignación donde modifica\nel estado de memoria del ordenador\ny modificación a detalle.


@endmindmap
```
## Lenguaje de Programación Funcional (2015).
```plantuml
@startmindmap
*[#568b9e] Programación Funcional.
	*[#caa7cf] Funciones matemáticas.
		*_ Como consecuencias.
			*[#d1cab1] Concepto de asignación.
			*[#d1cab1] Recursión.
			*[#d1cab1] Definción variable, desaparece.
	*[#caa7cf] Ventajas
		*[#d1cab1] Estado de computo.
		*[#d1cab1] Retorna el mismo valor.
		*[#d1cab1] Parámetros de entrada.
		*[#d1cab1] Transparencia referencial.
		*[#d1cab1] Independientes al orden de cálculo.
	*[#caa7cf] Evaluaciones
		*[#d1cab1] Perezosa.
			*_ Estrategia
				*[#dee8d0] De evaluacion que retrasa\nel calculo de una expresion,\nevitar repetir la evaluacion.
		*[#d1cab1] No estricta.
			*_ Características.
				*[#dee8d0] Realizar la evaluacion de afuera hacia adentro.
				*[#dee8d0] No necesita el valor de\nparametros para desarrollarse.	
		*[#d1cab1] Corto circuito.
			*_ Características.
				*[#dee8d0] Denota la semantica de\nlos operadores booleanos en algunos\nlenguajes de programacion.
				*[#dee8d0] Segundo argumento no se\nejecuta o valúa si el primer argumento\nde la función AND evalúa y el resultado es falso

	*[#caa7cf] Memorización.
		*_ Por 
			*[#d1cab1] Donald Michie
				*_ En
					*[#dee8d0] 1968.
		*_ Almacena
			*[#d1cab1] El valor de una expresion\ncuya evaluacion fue realizada.	

		
	*[#caa7cf] Paradigma de programación.
		*_ Son el
			*[#d1cab1] Modelo de computación en\nlos que diferentes lenguajes dotan de\nsemántica a los programas.
		*_ Tipos
			*[#d1cab1] Imperativo
				*_ De tipos
					*[#dee8d0] Orientado a objetos.
						*_ Caracteristicas.
							*[#f7f9f8] Son pequeños trozos de codigos.
							*[#f7f9f8] Son los objetos que interactuan entre si.
							*[#f7f9f8] Se componen de instrucciones\nque se ejecutan secuencialmente.
					*[#dee8d0] Imperativo
						*_ Lo cual
							*[#f7f9f8] Su principal es dar ordenes.
			*[#d1cab1] Lógico.
				*_ Características.
					*[#dee8d0] Lógica simbólica.
					*[#dee8d0] Sentencias con respecto al programa.
		*_ Basados
			*[#d1cab1] En el modelo de Von Neumann
				*_ Diseñado por
					*[#dee8d0] El matemático John Von Neumann.
				*_ Propuso
					*[#dee8d0] Los programas se almacenaban en la misma maquina\nantes de ser ejecutados.
				*_ La ejecución 
					*[#dee8d0] serie de instrucciones\nque se ejecutarian secuencialmente.
						*_ Esta secuencia.
							*[#f7f9f8] Se podria modificar segun el estado del computo.
				*_ Tiene el
					*[#dee8d0] Estado de computo.
						*_ Características
							*[#f7f9f8] Zona de memoria que se\naccede mediante una serie de variables. 
							*[#f7f9f8] Almacena el valor de todas\nlas variables que estan definidas.
							*[#f7f9f8] Puden ser modificadas\nmediante la instruccion de asignacion.
				*_ En ocasiones.
					*[#dee8d0] Se le denomina imperativo, por las\nordenes que se le da al programa.
	*[#caa7cf] Currificación.
		*[#d1cab1] Técnica inventada
			*_ Por
				*[#dee8d0] Moses Schönfinkel y Gottlob Frege.
		*[#d1cab1] Referencia
			*_ Al
				*[#dee8d0] Lógico de Haskell Curry.
		*[#d1cab1] Aplicación parcial
			*_ Consiste
				*[#dee8d0] Varias funciones de entrada y\nde vuelve diferentes funciones.

	*[#caa7cf] Definir computación es.
		*_ Basarse con el concepto de
			*[#d1cab1] Lambda Calculo.
				*_ Desarrollado por
					*[#dee8d0] Alonzo Church y Sthepen Kleene.
						*_ En
							*[#f7f9f8] La década de 1930.
				*_ Es
					*[#dee8d0] Un sistema computacional\nequivalente al modelo Vonn Neumann.
				*_ Además
					*[#dee8d0] Sento las bases de la programacion funcional.

@endmindmap
```
### Elaborado por: Gonzalez Pascual Melvin Paul.
### Materia: Programación Lógica y Funcional.
